FROM accetto/ubuntu-vnc-xfce-g3:latest

USER root

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y wget unzip screen
RUN wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip -O /root/ngrok.zip
RUN unzip /root/ngrok.zip -d /root
RUN /root/ngrok authtoken 21rZWzJ5VExUjSjT4ltFAeolKZJ_xgWwUQnJdVXpufktnw6Q

ENTRYPOINT screen -dmS ngrok_novnc /root/ngrok tcp 6901 --region=ap --log="stdout" && /usr/bin/tini -- /dockerstartup/startup.sh